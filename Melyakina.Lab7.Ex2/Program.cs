﻿using System;
using System.Linq;

namespace Melyakina.Lab7.Ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Дана строка символов, содержащая некоторый текст. Разработать программу, которая
            //определяет, является ли данный текст палиндромом, т.е.читается ли он слева направо
            // так же, как и справа налево(например, «А роза упала на лапу Азора»).(без циклов)
                
                var input = Console.ReadLine();
                var output = new string(input.Reverse().ToArray());
           
                bool ignoreCase = true;
                
                bool result;
                if (ignoreCase)
                {
                    result = output.Equals(input, StringComparison.InvariantCultureIgnoreCase);
                }
                else
                {
                    result = output.Equals(input);
                }

              Console.WriteLine(result);
        }
    }
}
