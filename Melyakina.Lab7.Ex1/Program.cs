﻿using System;

namespace Melyakina.Lab7.Ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Дана строка символов.Сформировать новую строку, в которую включить все символы
            //исходной строки, стоящие на четных местах. При этом должен быть обратный порядок
            //следования символов по отношения к исходной строке.
            string str = Console.ReadLine();
            char[] s = str.ToCharArray();

            string Result = "";
            for (int i = 0; i <=s.Length/2 ; i++)
            {
               
               if (i % 2 == 0)
               {
                    Result = s[i * 2] + Result;
               }
            }
            Console.Write($"{Result}");
             
        } 
    }   
}
