﻿using System;

namespace Melyakina.Lab7.Ex2.v2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Дана строка символов, содержащая некоторый текст. Разработать программу, которая
            //определяет, является ли данный текст палиндромом, т.е.читается ли он слева направо
            // так же, как и справа налево(например, «А роза упала на лапу Азора»).(с циклами)

            string str = Console.ReadLine();
            Console.WriteLine(IsPalindrom(str));


            static bool IsPalindrom(string word, bool ignoreCase = true)
            {
                if (ignoreCase)
                {
                    word = word.ToLowerInvariant();
                }

                for (int first = 0, last = word.Length - 1; first < last; ++first, --last)
                {
                    if (word[first] != word[last])
                    {
                        return false;
                    }
                }
                return true;
                
            }
        }
    }
}
