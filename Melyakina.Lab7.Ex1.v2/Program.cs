﻿using System;
using System.Linq;

namespace Melyakina.Lab7.Ex1.v2
{
    class Program
    {
        static void Main(string[] args)
        { //Дана строка символов.Сформировать новую строку, в которую включить все символы
          //исходной строки, стоящие на четных местах. При этом должен быть обратный порядок
          //следования символов по отношения к исходной строке.

            string InputString = Console.ReadLine();
          
            string OutputString = InputString.Where((c, i) => i % 2 == 0).Aggregate("", (str, c) => str += c);
            Console.WriteLine(OutputString.ToCharArray().Reverse().ToArray());

            Console.ReadKey();


        }
    }
}
